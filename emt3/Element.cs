﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace emt3
{
    class Element
    {
        public ElementType EtType;
        public double Quality = 0;

        public Element(ElementType etType)
        {
            EtType = etType;
            Quality = 0;
        }
    }
}
