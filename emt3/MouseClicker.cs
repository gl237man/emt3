﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace emt3
{
    [Flags]
    public enum MouseEventFlags
    {
        LEFTDOWN = 0x00000002,
        LEFTUP = 0x00000004,
        MIDDLEDOWN = 0x00000020,
        MIDDLEUP = 0x00000040,
        MOVE = 0x00000001,
        ABSOLUTE = 0x00008000,
        RIGHTDOWN = 0x00000008,
        RIGHTUP = 0x00000010
    }
    

    class MouseClicker
    {
        [DllImport("user32.dll", SetLastError = true)]
        public static extern void mouse_event(uint dwFlags, int dx, int dy, uint dwData, int dwExtraInfo);

        public static float SCH = 1050f;
        public static float SCL = 1680f;

        public static int SystemRessTime = 100;

        public static void LeftClick(float x, float y)
        {
            int dx = Convert.ToInt32(65536f / SCL * x);
            int dy = Convert.ToInt32(65536f / SCH * y);

            //System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.MOVE | (uint)MouseEventFlags.ABSOLUTE, dx, dy, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.LEFTUP, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
        }

        public static void LeftDragClickDelta(float x, float y,float DeltaX,float DeltaY)
        {
            int dx = Convert.ToInt32(65536f / SCL * x);
            int dy = Convert.ToInt32(65536f / SCH * y);

            int ddx = Convert.ToInt32(65536f / SCL * (x+DeltaX));
            int ddy = Convert.ToInt32(65536f / SCH * (y+DeltaY));

            //System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.MOVE | (uint)MouseEventFlags.ABSOLUTE, dx, dy, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.MOVE | (uint)MouseEventFlags.ABSOLUTE, ddx, ddy, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
            mouse_event((uint)MouseEventFlags.LEFTUP, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(SystemRessTime);
        }

        private static void ClickTest()
        {
            int dx = Convert.ToInt32( 65536f / SCL * 20f) ;
            int dy = Convert.ToInt32(65536f / SCH * 1025f);
            //mouse_event((uint)MouseEventFlags.MOVE, -5000, -5000, 0, 0);
            System.Threading.Thread.Sleep(100);

            mouse_event((uint)MouseEventFlags.MOVE | (uint)MouseEventFlags.ABSOLUTE, dx, dy, 0, 0);
            System.Threading.Thread.Sleep(100);
            mouse_event((uint)MouseEventFlags.LEFTDOWN, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(100);
            mouse_event((uint)MouseEventFlags.LEFTUP, 0, 0, 0, 0);
            System.Threading.Thread.Sleep(100);
            
        }


    }
}
