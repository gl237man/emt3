﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace emt3
{
    class Pole
    {
        public Element[,] Elements;
        public int Width;
        public int Height;
        public Pole(int width, int height)
        {
            Width = width;
            Height = height;
            Elements = new Element[width,height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Elements[x,y] = new Element(ElementType.none);
                }
            }
        }

        public Pole GetCopy()
        {
            Pole pole = new Pole(Width,Height);

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    pole.Elements[x, y]= new Element(Elements[x,y].EtType);
                }
            }
            return pole;
        }

        public bool Compare(Pole toComparePole)
        {

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (Elements[x, y].EtType != toComparePole.Elements[x, y].EtType) return false;
                }
            }
            return true;
        }

        public void DrawPole()
        {
            Console.WriteLine();
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    switch (Elements[x,y].EtType)
                    {
                        case ElementType.blue:
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.Write("B");
                            break;
                        case ElementType.gray:
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.Write("G");
                            break;
                        case ElementType.green:
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.Write("G");
                            break;
                        case ElementType.orange:
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            Console.Write("O");
                            break;
                        case ElementType.red:
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("R");
                            break;
                        case ElementType.yellow:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write("Y");
                            break;
                        case ElementType.pink:
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.Write("P");
                            break;
                        default:
                            Console.Write(" ");
                            break;
                            
                    }
                }
                Console.WriteLine();
            }
        }

        internal HodResult GetHod(int x1, int y1, int x2, int y2, int hlevel)
        {
            int score = 0;
            var resultPole = this.GetCopy();
            Element e1 = resultPole.Elements[x1, y1];
            Element e2 = resultPole.Elements[x2, y2];

            resultPole.Elements[x2, y2] = e1;
            resultPole.Elements[x1, y1] = e2;
            List<Point> elList = new List<Point>();

            for (int y = 0; y < Height; y++)
            {
                int tline = 0;
                Element LastElement = new Element(ElementType.none);
                for (int x = 0; x < Width; x++)
                {
                    if (resultPole.Elements[x, y].EtType == LastElement.EtType)
                    {
                        tline++;
                    }
                    else
                    {
                        if (tline > 1)
                        {
                            resultPole.DrawPole();
                            score += tline*100;
                            for (int i = 0; i < tline+1; i++)
                            {
                                elList.Add(new Point(x-i-1,y));
                            }
                        }
                        tline = 0;
                    }
                    if (x == Width - 1)
                    {
                        if (tline > 1)
                        {
                            resultPole.DrawPole();
                            score += tline * 100;
                            for (int i = 0; i < tline + 1; i++)
                            {
                                elList.Add(new Point(x - i - 1, y));
                            }
                        }
                        tline = 0;
                    }
                    LastElement = new Element(resultPole.Elements[x, y].EtType);
                }
            }

            for (int x = 0; x < Width; x++)
            {
                int tline = 0;
                Element LastElement = new Element(ElementType.none);
                for (int y = 0; y < Height; y++)
                {
                    if (resultPole.Elements[x, y].EtType == LastElement.EtType)
                    {
                        tline++;
                    }
                    else
                    {
                        if (tline > 1)
                        {
                            resultPole.DrawPole();
                            score += tline * 100;
                            for (int i = 0; i < tline + 1; i++)
                            {
                                elList.Add(new Point(x, y - i - 1));
                            }
                        }
                        tline = 0;
                    }
                    if (y == Height - 1)
                    {
                        if (tline > 1)
                        {
                            resultPole.DrawPole();
                            score += tline * 100;
                            for (int i = 0; i < tline + 1; i++)
                            {
                                elList.Add(new Point(x, y - i - 1));
                            }
                        }
                        tline = 0;
                    }
                    LastElement = new Element(resultPole.Elements[x, y].EtType);
                }
            }

            if (score > 0)
            {
                //Processing and recalc;
                return new HodResult {APoint = new Point(x1, y1),BPoint = new Point(x2, y2),level = hlevel,PoleResult = resultPole ,Score = score};
            }

            //resultPole.DrawPole();

            return null;
            //throw new NotImplementedException();
        }
    }
}
