﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;


namespace emt3
{
    class Program
    {
        public static bool DebugMode = true;

        public static Point PoleStart;

        static void Main(string[] args)
        {

            //string file = "test7.png";

            System.Threading.Thread.Sleep(10000);

            
            
            

            //i1.Save("t2.png");


            int safeZone = 6;
            double quality = 0.91;
            int cellSize = 80;

            int logicDeep = 2;

            while (true)
            {
                try
                {

                
            Stopwatch watch = new Stopwatch();

            watch.Start();
            //var Lastpole = GetBestScan(safeZone, cellSize, quality);
            var Lastpole = GetFastScan(safeZone, cellSize, quality);
            watch.Stop();
            Console.WriteLine("Time {0}ms",watch.ElapsedMilliseconds);
            Lastpole.DrawPole();

            int w = Lastpole.Width;
            int h = Lastpole.Height;


                    


                        List<HodResult> hods = new List<HodResult>();

                        //Опробовать все ходы

                        //Проверить валидность ходов и оценить их
                        for (int x = 0; x < w - 1; x++)
                        {
                            for (int y = 0; y < h; y++)
                            {
                                HodResult result = Lastpole.GetHod(x, y, x + 1, y, 0);
                                if (result != null)
                                    hods.Add(result);
                            }
                        }

                        for (int x = 0; x < w; x++)
                        {
                            for (int y = 0; y < h - 1; y++)
                            {
                                HodResult result = Lastpole.GetHod(x, y, x, y + 1, 0);
                                if (result != null)
                                    hods.Add(result);
                            }
                        }
                   

                        int maxpoint = hods.Select(t => t.Score).Max();
                        HodResult firstBestHod = hods.First(t => t.Score == maxpoint);

                        //var BestHods = hods.Where(t => t.Score == maxpoint).ToList();
                        var BestHods = hods.OrderBy(t => t.Score).Reverse().ToList();

                        for (int q = 0; q < BestHods.Count & q<6; q++)
                        {

                            firstBestHod = BestHods[q];

                        firstBestHod.PoleResult.DrawPole();

                        //Clic
                        MouseClicker.LeftClick(firstBestHod.APoint.X*cellSize + PoleStart.X + cellSize,
                            firstBestHod.APoint.Y*cellSize + PoleStart.Y + cellSize);
                        System.Threading.Thread.Sleep(30);
                        MouseClicker.LeftClick(firstBestHod.BPoint.X*cellSize + PoleStart.X + cellSize,
                            firstBestHod.BPoint.Y*cellSize + PoleStart.Y + cellSize);
                        System.Threading.Thread.Sleep(30);

                        }
                    MouseClicker.LeftClick(- 2 * cellSize + PoleStart.X + cellSize, - 1 * cellSize + PoleStart.Y + cellSize);
            
                }
                catch
                {
               
                }
            }
        }

        private static Pole GetBestScan(int safeZone, int cellSize, double quality)
        {
            Bitmap bm = ScreenShot.GetDisplayBitmap();
            var observedImage = new Image<Rgb, byte>(bm);
            var Lastpole = ScanPole(observedImage, safeZone, cellSize, quality);
            Lastpole.DrawPole();
            bool additionalscan = true;
            while (additionalscan)
            {
                bm = ScreenShot.GetDisplayBitmap();
                observedImage = new Image<Rgb, byte>(bm);
                var newpole = ScanPole(observedImage, safeZone, cellSize, quality);
                additionalscan = !Lastpole.Compare(newpole);
                Lastpole = newpole;
                Lastpole.DrawPole();
            }
            return Lastpole;
        }

        private static Pole GetFastScan(int safeZone, int cellSize, double quality)
        {
            Bitmap bm = ScreenShot.GetDisplayBitmap();
            var observedImage = new Image<Rgb, byte>(bm);
            var Lastpole = ScanPole(observedImage, safeZone, cellSize, quality);
            Lastpole.DrawPole();
            return Lastpole;
        }

        private static Pole ScanPole(Image<Rgb, byte> observedImage, int safeZone, int cellSize, double quality)
        {
            Image i1 = new Bitmap(1,1);
            var redim = new Image<Rgb, byte>("red.png");
            var greenim = new Image<Rgb, byte>("green.png");
            var yellowim = new Image<Rgb, byte>("yellow.png");
            var blueim = new Image<Rgb, byte>("blue.png");
            var orangeim = new Image<Rgb, byte>("orange.png");
            var grayim = new Image<Rgb, byte>("gray.png");
            var pinkim = new Image<Rgb, byte>("pink.png");
            var markerim = new Image<Rgb, byte>("marker.png");
            

            if (DebugMode)
            {
                i1 = new Bitmap(observedImage.Bitmap);
            }

            var resultList = FindAll(observedImage, markerim, 0.999, safeZone);

            if (DebugMode)
            {
                foreach (var r in resultList)
                {
                    Graphics.FromImage(i1).DrawRectangle(Pens.Gold, r.Rec);
                }
            }
            Point topLeft = new Point(9999, 999);
            Point downRight = new Point(0, 0);

            foreach (var r in resultList)
            {
                if (r.Rec.Location.X <= topLeft.X)
                {
                    topLeft.X = r.Rec.Location.X;
                }
                if (r.Rec.Location.Y <= topLeft.Y)
                {
                    topLeft.Y = r.Rec.Location.Y;
                }
                if (r.Rec.Location.X >= downRight.X)
                {
                    downRight.X = r.Rec.Location.X;
                }
                if (r.Rec.Location.Y >= downRight.Y)
                {
                    downRight.Y = r.Rec.Location.Y;
                }
            }

            Rectangle gameDesk = new Rectangle(topLeft.X, topLeft.Y, downRight.X - topLeft.X, downRight.Y - topLeft.Y);
            Graphics.FromImage(i1).DrawRectangle(Pens.Gold, gameDesk);

            PoleStart = new Point(gameDesk.X,gameDesk.Y);

            var roi = new Rectangle(gameDesk.X, gameDesk.Y, gameDesk.Width + cellSize, gameDesk.Height + cellSize);

            observedImage.ROI = roi;

            //Создание доски

            Pole pole = new Pole(gameDesk.Width/cellSize, gameDesk.Height/cellSize);

            resultList = FindAll(observedImage, redim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Red, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;

                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.red);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            resultList = FindAll(observedImage, blueim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Blue, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;
                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.blue);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            resultList = FindAll(observedImage, orangeim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Orange, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;
                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.orange);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            resultList = FindAll(observedImage, greenim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Green, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;
                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.green);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            resultList = FindAll(observedImage, yellowim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Yellow, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;
                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.yellow);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            resultList = FindAll(observedImage, grayim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Gray, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;
                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.gray);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            resultList = FindAll(observedImage, pinkim, quality, safeZone);

            foreach (var r in resultList)
            {
                if (DebugMode)
                {
                    var debugR = new Rectangle(gameDesk.X + r.Rec.X, gameDesk.Y + r.Rec.Y, r.Rec.Width, r.Rec.Height);
                    Graphics.FromImage(i1).DrawRectangle(Pens.Gray, debugR);
                }
                //Adding Element At Pole
                int coordx = (r.Rec.X) / cellSize;
                int coordy = (r.Rec.Y) / cellSize;
                if (coordx >= 0 & coordy >= 0 & coordx < pole.Width & coordy < pole.Height)
                {
                    if (pole.Elements[coordx, coordy].Quality < r.qual)
                    {
                        pole.Elements[coordx, coordy] = new Element(ElementType.pink);
                        pole.Elements[coordx, coordy].Quality = r.qual;
                    }
                }
            }

            if (DebugMode)
            {
                i1.Save("debug.png");
            }
            return pole;
        }

        private static List<FindPoint> FindAll(Image<Rgb, byte> observedImage, Image<Rgb, byte> modelImage, double quality, int safeZone)
        {
            List<FindPoint> resultList;
            var result = observedImage.MatchTemplate(modelImage, TemplateMatchingType.CcorrNormed);
            int num = 0;
            double min = 0;
            double max = 2.0f;
            resultList = new List<FindPoint>();
            while (max > quality)
            {
                Point minpoint = new Point();
                Point maxpoint = new Point();

                CvInvoke.MinMaxLoc(result, ref min, ref max, ref minpoint, ref maxpoint);


                var r = new Rectangle(maxpoint.X - safeZone, maxpoint.Y - safeZone, modelImage.Width + safeZone,
                    modelImage.Height + safeZone);

                Point[] pts = new Point[]
                {
                    new Point(r.Left, r.Bottom),
                    new Point(r.Right, r.Bottom),
                    new Point(r.Right, r.Top),
                    new Point(r.Left, r.Top)
                };

                Console.WriteLine("num={2} x={0} y={1}", pts[0].X, pts[0].Y, num);

                result.FillConvexPoly(pts, new Gray(0), LineType.AntiAlias, 0);
                resultList.Add(new FindPoint {qual = max, Rec = r});
                num++;
            }
            return resultList;
        }
    }
}
