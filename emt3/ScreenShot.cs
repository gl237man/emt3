﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.IO;

namespace emt3
{
    class ScreenShot
    {
        [DllImport("gdi32.dll")]
        static extern bool BitBlt(
            IntPtr hdc,
            int nXDest,
            int nYDest,
            int nWidth,
            int nHeight,
            IntPtr hdcSrc,
            int nXSrc,
            int nYSrc,
            uint dwRop
            );

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleDC(
            IntPtr hdc
            );

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleBitmap(
            IntPtr hdc,
            int nWidth,
            int nHeight
            );

        [DllImport("gdi32.dll")]
        static extern bool DeleteObject(
            IntPtr hObject
            );

        [DllImport("gdi32.dll")]
        static extern bool DeleteDC(
            IntPtr hdc
            );

        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc,
            int nIndex
            );

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateDC(
            string lpszDriver,
            string lpszDevice,
            string lpszOutput,
            string lpInitData
            );

        [DllImport("gdi32.dll")]
        static extern IntPtr SelectObject(
            IntPtr hdc,
            IntPtr hgdiobj
            );

        [DllImport("kernel32")]
        static extern bool CloseHandle(
            IntPtr hFile
            );

        [DllImport("user32.dll")]
        static extern int ReleaseDC(
            IntPtr hWnd,
            IntPtr hDC
            );

        private struct RasterOperationCode
        {
            public const int BLACKNESS = 0x00000042;
            public const int NOTSRCERASE = 0x001100A6;
            public const int NOTSRCCOPY = 0x00330008;
            public const int SRCERASE = 0x00440328;
            public const int DSTINVERT = 0x00550009;
            public const int PATINVERT = 0x005A0049;
            public const int SRCINVERT = 0x00660046;
            public const int SRCAND = 0x008800C6;
            public const int MERGEPAINT = 0x00BB0226;
            public const int MERGECOPY = 0x00C000CA;
            public const int SRCCOPY = 0x00CC0020;
            public const int SRCPAINT = 0x00EE0086;
            public const int PATCOPY = 0x00F00021;
            public const int PATPAINT = 0x00FB0A09;
            public const int WHITENESS = 0x00FF0062;
        }

        private struct DeviceCapsIndex
        {
            public const int Width = 8;
            public const int Height = 10;
        }

        public static Bitmap GetDisplayBitmap()
        {
            IntPtr hDesktopDC = CreateDC("DISPLAY", "", "", "");
            int Width = GetDeviceCaps(hDesktopDC, DeviceCapsIndex.Width);
            int Height = GetDeviceCaps(hDesktopDC, DeviceCapsIndex.Height);
            IntPtr hMyDC = CreateCompatibleDC(hDesktopDC);
            IntPtr hBitmap = CreateCompatibleBitmap(hDesktopDC, Width, Height);
            IntPtr hOldBmp = SelectObject(hMyDC, hBitmap);
            BitBlt(hMyDC, 0, 0, Width, Height, hDesktopDC, 0, 0, RasterOperationCode.SRCCOPY);
            Bitmap DisplayBitmap = Bitmap.FromHbitmap(hBitmap);
            return DisplayBitmap;
        }

        public static int[,] GetDisplayPixelArray()
        {
            IntPtr hDesktopDC = CreateDC("DISPLAY", "", "", "");
            int Width = GetDeviceCaps(hDesktopDC, DeviceCapsIndex.Width);
            int Height = GetDeviceCaps(hDesktopDC, DeviceCapsIndex.Height);
            IntPtr hMyDC = CreateCompatibleDC(hDesktopDC);
            IntPtr hBitmap = CreateCompatibleBitmap(hDesktopDC, Width, Height);
            IntPtr hOldBmp = SelectObject(hMyDC, hBitmap);
            BitBlt(hMyDC, 0, 0, Width, Height, hDesktopDC, 0, 0, RasterOperationCode.SRCCOPY);
            Bitmap DisplayBitmap = Bitmap.FromHbitmap(hBitmap);
            int[,] Pixels = new int[Width, Height];
            using (MemoryStream ms = new MemoryStream())
            {
                /*DisplayBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                byte[] Bytes = ms.ToArray();
                long pos = Bytes.Length;
                for (int j = 0; j < Height; j++)
                {
                    for (int i = Width; i-- > 0; )
                    {
                        Pixels[i, j] =
                            (Bytes[--pos] << 24) |
                            (Bytes[--pos] << 16) |
                            (Bytes[--pos] << 8) |
                            (Bytes[--pos]);
                    }
                }
                Bytes = null;*/
                System.Drawing.Imaging.EncoderParameters EP = new System.Drawing.Imaging.EncoderParameters(2);
                EP.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, 24L);
                //EP.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.RenderMethod, (long)System);
                EP.Param[1] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)System.Drawing.Imaging.EncoderValue.CompressionNone);
                //EP.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder., 32);
                //System.Drawing.Imaging.ImageCodecInfo[] info0 = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
                System.Drawing.Imaging.ImageCodecInfo info;
                info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()[3];
                //info = System.Drawing.Imaging.
                //System.Drawing.Imaging.Encoder = new System.Drawing.Imaging.Encoder(System.Drawing.Imagi
                //System.Drawing.Imaging.EncoderParameter p1 = new System.Drawing.Imaging.EncoderParameter(
                DisplayBitmap.Save(ms, info, EP);
                //DisplayBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                byte[] Bytes = ms.ToArray();
                long pos = Bytes.Length;
                pos = 7;
                for (int j = 0; j < DisplayBitmap.Height; j++)
                {
                    //for (int i = DisplayBitmap.Width; i-- > 0; )
                    for (int i = 0; i < DisplayBitmap.Width; i++)
                    {
                        Pixels[i, j] =
                            (Bytes[++pos] << 16) |
                            (Bytes[++pos] << 8) |
                            (Bytes[++pos] << 0)
                            ;//(Bytes[++pos] << 0);
                    }
                }
                Bytes = null;
            }
            DisplayBitmap.Dispose();
            DeleteObject(SelectObject(hMyDC, hOldBmp));
            DeleteDC(hMyDC);
            DeleteDC(hDesktopDC);
            //CloseHandle(hBitmap);
            //CloseHandle(hOldBmp);
            ReleaseDC(IntPtr.Zero, hMyDC);
            ReleaseDC(IntPtr.Zero, hDesktopDC);
            return Pixels;
        }

        public static int[,] GetPixelArryFromFile(string FileName)
        {

            Bitmap DisplayBitmap =new  Bitmap (FileName);
            int[,] Pixels = new int[DisplayBitmap.Width, DisplayBitmap.Height];
            using (MemoryStream ms = new MemoryStream())
            {
                System.Drawing.Imaging.EncoderParameters EP = new System.Drawing.Imaging.EncoderParameters(2);
                EP.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, 24L);
                //EP.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.RenderMethod, (long)System);
                EP.Param[1] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression,(long)System.Drawing.Imaging.EncoderValue.CompressionNone);
                //EP.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder., 32);
                //System.Drawing.Imaging.ImageCodecInfo[] info0 = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
                System.Drawing.Imaging.ImageCodecInfo info;
                info = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()[3];
                //info = System.Drawing.Imaging.
                //System.Drawing.Imaging.Encoder = new System.Drawing.Imaging.Encoder(System.Drawing.Imagi
                //System.Drawing.Imaging.EncoderParameter p1 = new System.Drawing.Imaging.EncoderParameter(
                DisplayBitmap.Save(ms,info,EP);
                //DisplayBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                byte[] Bytes = ms.ToArray();
                long pos = Bytes.Length;
                pos = 7;
                for (int j = 0; j < DisplayBitmap.Height; j++)
                {
                    //for (int i = DisplayBitmap.Width; i-- > 0; )
                    for (int i = 0; i < DisplayBitmap.Width ;i++ )
                    {
                        Pixels[i, j] =
                            (Bytes[++pos] << 16) |
                            (Bytes[++pos] << 8) |
                            (Bytes[++pos] << 0) 
                            ;//(Bytes[++pos] << 0);
                    }
                }
                Bytes = null;
            }
            DisplayBitmap.Dispose();

            return Pixels;
        }
    }
}
